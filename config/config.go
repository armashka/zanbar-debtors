package config

import (
	"gopkg.in/yaml.v3"
	"os"
	"time"
)

type Config struct {
	Server struct {
		Port    string `yaml:"port"`
		Host    string `yaml:"host"`
		Timeout struct {
			Server time.Duration `yaml:"server"`
			Write  time.Duration `yaml:"write"`
			Read   time.Duration `yaml:"read"`
			Idle   time.Duration `yaml:"idle"`
		} `yaml:"timeout"`
	} `yaml:"server"`

	Business struct {
		Count int `yaml:"inn_count"`
	} `yaml:"business"`

	Egov struct {
		ApiKey      string `yaml:"api_key"`
		ContentType string `yaml:"content_type"`
		EgovUrl     string `yaml:"egov_url"`
		IndexName   string `yaml:"index_name"`
		FoundKey    string `yaml:"found_key"`
	} `yaml:"egov"`

	Push struct {
		Url string `yaml:"url"`
	} `yaml:"push"`

	MongoDB struct {
		Host            string `yaml:"host"`
		Port            string `yaml:"port"`
		DBName          string `yaml:"db_name"`
		UserCollection  string `yaml:"user_collection"`
		DebtsCollection string `yaml:"debts_collection"`
		DBUser          string `yaml:"user_name"`
		DBPassword      string `yaml:"user_pass"`
	} `yaml:"mongo_db"`
}

// NewConfig returns a new decoded Config struct
func NewConfig(configPath string) (*Config, error) {
	// Create config structure
	config := &Config{}

	// Open config file
	file, err := os.Open(configPath)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	// Init new YAML decode
	d := yaml.NewDecoder(file)

	// Start YAML decoding from file
	if err := d.Decode(&config); err != nil {
		return nil, err
	}

	return config, nil
}
