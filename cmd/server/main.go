package main

import (
	"context"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zanbar-debtors/config"
	"gitlab.com/zanbar-debtors/internal"
	"gitlab.com/zanbar-debtors/internal/api/rest"
	"gitlab.com/zanbar-debtors/internal/egov"
	mon "gitlab.com/zanbar-debtors/internal/mongo"
	"gitlab.com/zanbar-debtors/internal/parser"
	fcm "gitlab.com/zanbar-debtors/internal/push"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {

	// register logger
	logger := log.New()
	logger.SetFormatter(&log.JSONFormatter{})

	// get configs
	conf, err := config.NewConfig("./config/conf.yml")
	if err != nil {
		logger.Fatalln(err)
	}

	// connect mongo
	client, err := connectMongo(conf.MongoDB.Host, conf.MongoDB.Port, conf.MongoDB.DBUser, conf.MongoDB.DBPassword)
	if err != nil {
		logger.Fatalln(err)
	}

	// register rep
	rep := mon.NewRep(client, conf.MongoDB.DBName, conf.MongoDB.UserCollection, conf.MongoDB.DebtsCollection, logger)

	// push notifications
	push := fcm.NewSt(conf.Push.Url)

	// register parser
	ps := parser.NewParser(rep, push, "announcement", logger, conf.Egov.ApiKey, conf.Egov.ContentType, conf.Egov.EgovUrl, conf.Egov.IndexName, conf.Egov.FoundKey)

	// register useCase
	useCase := egov.New(rep, ps, conf.Egov.ApiKey, conf.Egov.ContentType, conf.Egov.EgovUrl, conf.Egov.IndexName, conf.Egov.FoundKey, conf.Business.Count, logger)

	// register a ticker
	ticker1 := time.NewTicker(24 * time.Hour)
	notifier := make(chan struct{})
	// goroutine which will trigger parse method
	// run goroutine
	go ticker(ps, ticker1, notifier)

	// server init
	srv := rest.NewServer(*conf, logger, useCase)

	logger.Infof("server started on port: %s", conf.Server.Port)

	srv.Start()

	stop := make(chan os.Signal, 1)
	signal.Notify(stop, os.Interrupt, syscall.SIGTERM, syscall.SIGINT)

	var exitCode int

RunLoop:
	for {
		select {
		case <-notifier:
			fmt.Println("restart ticker")
			go ticker(ps, ticker1, notifier)
		case <-stop:
			break RunLoop
		case <-srv.Wait():
			exitCode = 1
			break RunLoop
		}
	}

	logger.Info("Shutting down server...")

	restApiShutdownCtx, restApiShutdownCtxCancel := context.WithTimeout(context.Background(), 20*time.Second)
	defer restApiShutdownCtxCancel()

	err = srv.Shutdown(restApiShutdownCtx)
	if err != nil {
		logger.Errorln("Fail to shutdown http-api", err)
		exitCode = 1
	}

	os.Exit(exitCode)
}

func ticker(parser internal.Parser, ticker *time.Ticker, notifier chan struct{}) {
	defer func(notifier chan struct{}) {
		notifier <- struct{}{}
	}(notifier)
	for {
		<-ticker.C
		parser.Parse()
	}
}

func connectMongo(host, port, user, pass string) (*mongo.Client, error) {
	clientOptions := options.Client().ApplyURI(fmt.Sprintf("mongodb://%s:%s@%s:%s", user, pass, host, port))
	// Connect to MongoDB
	rep, err := mongo.Connect(context.TODO(), clientOptions)
	if err != nil {
		// proper
		return nil, err
	}
	return rep, nil
}
