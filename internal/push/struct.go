package fcm

type sendReqSt struct {
	UsrIds     []int64           `json:"usr_ids"`
	Title      string            `json:"title"`
	Body       string            `json:"body"`
	Data       map[string]string `json:"data"`
	Badge      int               `json:"badge"`
	AndroidTag string            `json:"android_tag"`
}
