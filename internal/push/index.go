package fcm

import (
	"bytes"
	"encoding/json"
	"log"
	"net/http"
	"time"
)

const conTimeout = 20 * time.Second

type St struct {
	url string

	httpClient *http.Client
}

func NewSt(url string) *St {
	return &St{
		url:        url,
		httpClient: &http.Client{Timeout: conTimeout},
	}
}

func (p *St) Send2User(usrId int64, title, body string, data map[string]string, badge int, androidTag string) bool {
	return p.Send2Users([]int64{usrId}, title, body, data, badge, androidTag)
}

func (p *St) Send2Users(usrIds []int64, title, body string, data map[string]string, badge int, androidTag string) bool {
	if len(usrIds) == 0 {
		return true
	}

	reqBytes, err := json.Marshal(sendReqSt{
		UsrIds:     usrIds,
		Title:      title,
		Body:       body,
		Data:       data,
		Badge:      badge,
		AndroidTag: androidTag,
	})
	if err != nil {
		log.Println("Fail to marshal json", err)
		return false
	}

	req, err := http.NewRequest("POST", p.url+"send", bytes.NewBuffer(reqBytes))
	if err != nil {
		log.Println("Fail to create http-request", err)
		return false
	}

	rep, err := p.httpClient.Do(req)
	if err != nil {
		log.Println("Fail to send http-request", err)
		return false
	}
	defer rep.Body.Close()

	if rep.StatusCode < 200 || rep.StatusCode >= 300 {
		log.Println("Fail to send http-request, bad status code", nil, "status_code", rep.StatusCode)
		return false
	}

	return true
}
