package parser

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/zanbar-debtors/internal"
	"gitlab.com/zanbar-debtors/internal/egov"
	fcm "gitlab.com/zanbar-debtors/internal/push"
	"gitlab.com/zanbar-debtors/pkg/utils"
	"strconv"
	"time"
)

type parser struct {
	reps        internal.Reps
	push        *fcm.St
	channelName string

	logg        *log.Entry
	apiKey      string
	contentType string
	egovUrl     string
	indexName   string
	foundKey    string
}

func NewParser(reps internal.Reps, push *fcm.St, channelName string, logg *log.Logger, apiKey string, contentType string, egovUrl string, indexName string, foundKey string) internal.Parser {

	loggerContext := logg.WithFields(log.Fields{
		"LEVEL": "Parser",
	})

	return &parser{
		reps:        reps,
		push:        push,
		channelName: channelName,
		logg:        loggerContext,
		apiKey:      apiKey,
		contentType: contentType,
		egovUrl:     egovUrl,
		indexName:   indexName,
		foundKey:    foundKey,
	}
}

func (p *parser) Parse() {
	p.logg.WithFields(log.Fields{
		"FUNC": "Parse",
	}).Info("started notification at " + time.Now().String())

	iin, err := p.reps.GetDistinctIIN()
	if err != nil {
		p.logg.WithFields(log.Fields{
			"FUNC": "Parse/GetDistinctINN",
		}).Error("could not get distinct IINs major error")
		return
	}

	p.logg.WithFields(log.Fields{
		"FUNC": "Parse",
	}).Info("distinct iins: ", iin)

	for _, val := range iin {
		debts, err := p.reps.GetDebts(val)
		if err != nil {
			p.logg.WithFields(log.Fields{
				"FUNC": "Parse/GetDebts",
			}).Error("could not get debts by IINs : %s", val)
			continue
		}

		// make request
		request, err := egov.MakeRequest(val, p.indexName, p.apiKey, p.egovUrl, p.contentType, p.logg)
		if err != nil {
			p.logg.WithFields(log.Fields{
				"FUNC": "Parse/GetDebts",
			}).Error("could not make request by IINs : %s", val)
			continue
		}
		rows := request.Body.RequestResponse.Response.ResponseData.Data.Rows
		// calculate new hash
		newHash := utils.AsSha256(rows)

		if newHash != debts.Hash {
			// notify
			err := p.Notify(val, -1)
			if err != nil {
				p.logg.WithFields(log.Fields{
					"FUNC": "Parse/Notify",
				}).Error("could not Notify by IINs : %s", val)
				continue
			}

			// update
			debts.Rows = rows
			debts.Hash = newHash
			err = p.reps.SaveDebts(debts)
			if err != nil {
				p.logg.WithFields(log.Fields{
					"FUNC": "Parse/Save Updates",
				}).Error("could update debts IINs : %s", val)
				continue
			}
		}

	}

	p.logg.WithFields(log.Fields{
		"FUNC": "Parse",
	}).Info("finished notification at " + time.Now().String())
}

func (p *parser) Notify(v string, id int64) error {
	byIIN, err := p.reps.GetUserIDByIIN(v)
	if err != nil {
		p.logg.WithFields(log.Fields{
			"FUNC": "Notify/GetUserID",
		}).Error("could not get users listening IIN : %s", v)
		return err
	}
	// call for notify variables
	for _, val := range byIIN {
		if val == id {
			continue
		}
		var isSend bool
		for i := 0; i < 3 && !isSend; i++ {
			isSend = p.push.Send2User(val, "update in debts", "test check from Arman, user_id -> "+strconv.FormatInt(val, 10)+" iin -> "+v, map[string]string{}, 2, p.channelName)
		}
	}
	return nil
}
