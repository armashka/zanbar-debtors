package internal

import (
	"gitlab.com/zanbar-debtors/internal/entity"
)

type Parser interface {
	Parse()
	Notify(val string, id int64) error
}

type UseCase interface {
	GetDebts(iin string, userID int64) ([]*entity.RespDebtorsRegistry, error)
	GetRegisteredIINs(userID int64) (*entity.User, error)

	UpdateDebtsSingle(userID int64, iin, oldIin string, name string) (*entity.User, error)
	CreateDebtsSingle(userID int64, iin string, name string) (*entity.User, error)

	RemoveDebtsSingle(userID int64, iin string) (*entity.User, error)

	UpdateDebts(userID int64, IINs []*entity.IIN) (*entity.User, error)
	RemoveDebts(userID int64, IINs []string) (*entity.User, error)
}

type Reps interface {
	GetDistinctIIN() ([]string, error)

	GetUser(id int64) (*entity.User, error)
	SaveUser(user *entity.User) error

	GetDebts(iin string) (*entity.Debts, error)
	SaveDebts(data *entity.Debts) error

	// get user ids by IINs
	GetUserIDByIIN(iin string) ([]int64, error)
	GetDebtsList() ([]string, error)
}
