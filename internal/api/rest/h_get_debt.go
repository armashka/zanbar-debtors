package rest

import (
	"github.com/go-chi/chi"
	errors "gitlab.com/zanbar-debtors/internal/errors"
	"gitlab.com/zanbar-debtors/pkg/utils"
	"net/http"
	"strconv"
)

func (s *Server) hGetDebtByIIN(w http.ResponseWriter, r *http.Request) {

	value := chi.URLParam(r, "val")
	userID := chi.URLParam(r, "userID")

	parseInt, err := strconv.ParseInt(userID, 10, 64)
	if err != nil {
		s.transformError(errors.ErrBadFormat, w)
		return
	}

	debts, err := s.use.GetDebts(value, parseInt)
	if err != nil {
		s.transformError(err, w)
		return
	}

	utils.ResponseWithHttp(w, debts, 200, s.logg)
}
