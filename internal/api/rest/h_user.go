package rest

import (
	"encoding/json"
	"github.com/go-chi/chi"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zanbar-debtors/internal/entity"
	"gitlab.com/zanbar-debtors/internal/errors"
	"gitlab.com/zanbar-debtors/pkg/utils"
	"net/http"
	"strconv"
)

func (s *Server) hGetGetRegisteredIIN(w http.ResponseWriter, r *http.Request) {

	value := chi.URLParam(r, "val")
	parseInt, err := strconv.ParseInt(value, 10, 64)
	if err != nil {
		s.transformError(errors.ErrBadFormat, w)
		return
	}

	ns, err := s.use.GetRegisteredIINs(parseInt)
	if err != nil {
		s.transformError(err, w)
		return
	}

	utils.ResponseWithHttp(w, ns, 200, s.logg)
}

func (s *Server) hRegisterIINs(w http.ResponseWriter, r *http.Request) {

	req := &entity.User{}
	err := json.NewDecoder(r.Body).Decode(req)
	if err != nil {
		s.transformError(errors.ErrBadFormat, w)
		return
	}

	err = req.Validate()

	if err != nil {
		s.transformError(errors.ErrValidationFailed, w)
		return
	}

	debts, err := s.use.UpdateDebts(*req.UserID, req.IINs)
	if err != nil {
		s.transformError(err, w)
		return
	}

	utils.ResponseWithHttp(w, debts, 200, s.logg)
}

func (s *Server) hRegisterIIN(w http.ResponseWriter, r *http.Request) {

	req := &entity.UserRequestSingle{}
	err := json.NewDecoder(r.Body).Decode(req)
	if err != nil {
		s.transformError(errors.ErrBadFormat, w)
		return
	}

	err = req.Validate()
	if err != nil {
		s.transformError(errors.ErrValidationFailed, w)
		return
	}

	single, err := s.use.CreateDebtsSingle(*req.UserID, *req.IIN, *req.Name)
	if err != nil {
		s.transformError(err, w)
		return
	}

	utils.ResponseWithHttp(w, single, 200, s.logg)
}

func (s *Server) hUpdateRegisteredIINs(w http.ResponseWriter, r *http.Request) {

	req := &entity.User{}
	err := json.NewDecoder(r.Body).Decode(req)
	if err != nil {
		s.transformError(errors.ErrBadFormat, w)
		return
	}

	err = req.Validate()
	if err != nil {
		s.transformError(errors.ErrValidationFailed, w)
		return
	}

	single, err := s.use.UpdateDebts(*req.UserID, req.IINs)
	if err != nil {
		s.transformError(err, w)
		return
	}

	utils.ResponseWithHttp(w, single, 200, s.logg)
}

func (s *Server) hUpdateRegisteredIIN(w http.ResponseWriter, r *http.Request) {

	req := &entity.UserUpdateRequestSingle{}
	err := json.NewDecoder(r.Body).Decode(req)
	if err != nil {
		s.transformError(errors.ErrBadFormat, w)
		return
	}

	err = req.Validate()
	if err != nil {
		s.transformError(errors.ErrValidationFailed, w)
		return
	}

	single, err := s.use.UpdateDebtsSingle(*req.UserID, *req.IIN, *req.OldIIN, *req.Name)
	if err != nil {
		s.transformError(err, w)
		return
	}

	utils.ResponseWithHttp(w, single, 200, s.logg)
}

func (s *Server) hRemoveRegisteredIINs(w http.ResponseWriter, r *http.Request) {

	req := &entity.DeleteDebts{}
	err := json.NewDecoder(r.Body).Decode(req)
	if err != nil {
		s.transformError(errors.ErrBadFormat, w)
		return
	}

	err = req.Validate()
	if err != nil {
		s.transformError(errors.ErrValidationFailed, w)
		return
	}

	single, err := s.use.RemoveDebts(*req.UserID, req.INNs)
	if err != nil {
		s.transformError(err, w)
		return
	}

	utils.ResponseWithHttp(w, single, 200, s.logg)
}

func (s *Server) hRemoveRegisteredIIN(w http.ResponseWriter, r *http.Request) {

	req := &entity.UserDeleteRequestSingle{}
	err := json.NewDecoder(r.Body).Decode(req)
	if err != nil {
		s.transformError(errors.ErrBadFormat, w)
		return
	}

	err = req.Validate()
	if err != nil {
		s.transformError(errors.ErrValidationFailed, w)
		return
	}

	single, err := s.use.RemoveDebtsSingle(*req.UserID, *req.IIN)
	if err != nil {
		s.transformError(err, w)
		return
	}

	utils.ResponseWithHttp(w, single, 200, s.logg)
}

func (s *Server) transformError(err error, w http.ResponseWriter) {
	switch cErr := err.(type) {
	case *errors.Err:
		utils.ResponseWithHttpError(w, err, cErr.Code, s.logg)
	default:
		s.logg.WithFields(log.Fields{
			"Handler": "UNEXPECTED ERROR",
		}).Errorf("Error: %s", err)

		utils.ResponseWithHttpError(w, errors.ErrInternal, 500, s.logg)
	}
}
