package rest

import (
	"github.com/go-chi/chi"
	"net/http"
)

func (s *Server) NewRouter() http.Handler {

	router := chi.NewRouter()

	router.Route("/user/", func(r chi.Router) {

		r.Post("/iins", s.hRegisterIINs)
		r.Post("/iin", s.hRegisterIIN)

		r.Post("/remove/iin", s.hRemoveRegisteredIIN)
		r.Post("/remove/iins", s.hRemoveRegisteredIINs)

		r.Put("/iin", s.hUpdateRegisteredIIN)
		r.Put("/iins", s.hUpdateRegisteredIINs)

		r.Get("/iins/{val}", s.hGetGetRegisteredIIN)

	})

	router.Get("/debts/{userID}/{val}", s.hGetDebtByIIN)

	return s.middleware(router)
}
