package rest

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zanbar-debtors/config"
	"gitlab.com/zanbar-debtors/internal"
	"net/http"
)

type Server struct {
	server *http.Server
	lChan  chan error
	use    internal.UseCase

	logg *log.Entry
}

func NewServer(config config.Config, logg *log.Logger, use internal.UseCase) *Server {

	loggerContext := logg.WithFields(log.Fields{
		"LEVEL": "Handlers",
	})
	s := &Server{}
	s.server = &http.Server{
		Addr:         ":" + config.Server.Port,
		Handler:      s.NewRouter(),
		ReadTimeout:  config.Server.Timeout.Read,
		WriteTimeout: config.Server.Timeout.Write,
		IdleTimeout:  config.Server.Timeout.Idle,
	}
	s.lChan = make(chan error, 1)
	s.logg = loggerContext
	s.use = use

	return s
}

func (s *Server) Start() {
	go func() {
		err := s.server.ListenAndServe()
		if err != nil && err != http.ErrServerClosed {
			s.logg.Errorln("Http server closed", err)
			s.lChan <- err
		}
	}()
}

func (s *Server) Wait() <-chan error {
	return s.lChan
}

func (s *Server) Shutdown(ctx context.Context) error {
	return s.server.Shutdown(ctx)
}
