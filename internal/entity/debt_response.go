package entity

import "encoding/xml"

type SoapResponse struct {
	XMLName xml.Name `xml:"Envelope"`
	Body    *soapResponseBody
}

type soapResponseBody struct {
	XMLName         xml.Name `xml:"Body"`
	RequestResponse *requestResponse
}

type requestResponse struct {
	XMLName  xml.Name `xml:"requestResponse"`
	Response *requestResponseBody
}

type requestResponseBody struct {
	XMLName      xml.Name `xml:"response"`
	ResponseInfo *responseInfo
	ResponseData *responseData
}

type responseInfo struct {
	XMLName xml.Name `xml:"responseInfo"`
	Status  *responseInfoStatus
}

type responseInfoStatus struct {
	XMLName xml.Name `xml:"status"`
	Code    string   `xml:"code"`
	Message string   `xml:"message"`
}

type responseData struct {
	XMLName xml.Name `xml:"responseData"`
	Data    *responseDataData
}

type responseDataData struct {
	XMLName xml.Name `xml:"data"`
	Rows    []*Rows  `xml:"rows"`
}

type Debts struct {
	IIN  string  `bson:"IIN"`
	Hash string  `bson:"hash" json:"-"`
	Rows []*Rows `xml:"rows" bson:"rows"`
}

type Rows struct {
	//	XMLName xml.Name `xml:"rows"`
	Surname               string  `xml:"debtorSurname" bson:"debtorSurname"`
	Name                  string  `xml:"debtorName" bson:"debtorName"`
	SecondName            string  `xml:"debtorSecondname" bson:"debtorSecondname"`
	Iin                   string  `xml:"iinOrBin" bson:"iinOrBin"`
	BanStartAt            string  `xml:"banStartDate" bson:"banStartDate"`
	BanEndAt              string  `xml:"banEndDate" bson:"banEndDate"`
	IpStartDate           string  `xml:"ipStartDate" bson:"ipStartDate"`
	IlDate                string  `xml:"ilDate" bson:"ilDate"`
	IlOrganRu             string  `xml:"ilOrganRu" bson:"ilOrganRu"`
	IlOrganKZ             string  `xml:"ilOrganKz" bson:"ilOrganKz"`
	CategoryRu            string  `xml:"categoryRu" bson:"categoryRu"`
	CategoryKz            string  `xml:"categoryKz" bson:"categoryKz"`
	RecoveryAmount        float64 `xml:"recoveryAmount" bson:"recoveryAmount"`
	OffSurname            string  `xml:"officerSurname" bson:"officerSurname"`
	OffName               string  `xml:"officerName" bson:"officerName"`
	OffSecondName         string  `xml:"officerSecondname" bson:"officerSecondname"`
	DisaNameRu            string  `xml:"disaNameRu" bson:"disaNameRu"`
	DisaNameKz            string  `xml:"disaNameKz" bson:"disaNameKz"`
	DisaDepartmentNameRu  string  `xml:"disaDepartmentNameRu" bson:"disaDepartmentNameRu"`
	DisaDepartmentNameKz  string  `xml:"disaDepartmentNameKz" bson:"disaDepartmentNameKz"`
	DisaDepartmentAddress string  `xml:"disaDepartmentAddress" bson:"disaDepartmentAddress"`
	RecovererTypeRu       string  `xml:"recovererTypeRu" bson:"recovererTypeRu"`
	RecovererTypeKz       string  `xml:"recovererTypeKz" bson:"recovererTypeKz"`
	RecovererIin          string  `xml:"recovererIin" bson:"recovererIin"`
	RecovererBin          string  `xml:"recovererBin" bson:"recovererBin"`
	RecovererTitle        string  `xml:"recovererTitle" bson:"recovererTitle"`
}
