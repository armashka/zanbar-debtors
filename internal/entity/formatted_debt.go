package entity

import "time"

type RespDebtorsRegistry struct {
	InRegistry bool      `json:"inregistry"`
	Iin        string    `json:"iin"`
	Name       string    `json:"name"`
	Category   string    `json:"category"`
	Info       string    `json:"info"`
	Date       time.Time `json:"date"`
	Organ      string    `json:"organ"`
	Recoverer  string    `json:"recoverer"`
}
