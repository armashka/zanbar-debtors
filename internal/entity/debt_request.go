package entity

type ReqDebtorsRegistry struct {
	MessageId   string
	MessageDate string
	IndexName   string
	ApiKey      string
	IinOrBin    string
}
