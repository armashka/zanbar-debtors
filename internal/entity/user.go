package entity

import (
	errors "gitlab.com/zanbar-debtors/internal/errors"
)

type User struct {
	UserID *int64 `bson:"_id" json:"user_id"`
	IINs   []*IIN `bson:"IINs" json:"IINs"`
}

type DeleteDebts struct {
	UserID *int64   `json:"user_id"`
	INNs   []string `json:"IINs"`
}

func (d *DeleteDebts) Validate() error {
	if d.INNs == nil || d.UserID == nil {
		return errors.ErrSomethingMissing
	}

	if *d.UserID <= 0 {
		return errors.ErrUserIDNotValid
	}

	if len(d.INNs) == 0 {
		return errors.ErrIINRequired
	}

	return nil
}

type IIN struct {
	INN  *string `json:"IIN" bson:"IIN"`
	Name *string `json:"name" bson:"name"`
}

func (i *IIN) validate() error {
	if i.INN == nil || i.Name == nil {
		return errors.ErrSomethingMissing
	}
	if *i.Name == "" {
		return errors.ErrNameRequired
	}
	return nil
}

func (u *User) Validate() error {
	if u.IINs == nil || u.UserID == nil {
		return errors.ErrSomethingMissing
	}

	if *u.UserID <= 0 {
		return errors.ErrUserIDNotValid
	}

	if len(u.IINs) == 0 {
		return errors.ErrIINRequired
	}

	for _, val := range u.IINs {
		if err := val.validate(); err != nil {
			return err
		}
	}
	return nil
}

// UserRequestSingle add ID in Creation
// check in update and delete
type UserRequestSingle struct {
	UserID *int64  `json:"user_id"`
	IIN    *string `json:"IIN"`
	Name   *string `json:"name"`
}

type UserDeleteRequestSingle struct {
	UserID *int64  `json:"user_id"`
	IIN    *string `json:"IIN"`
}

func (ud *UserDeleteRequestSingle) Validate() error {
	if ud.IIN == nil || ud.UserID == nil {
		return errors.ErrSomethingMissing
	}

	if *ud.UserID <= 0 {
		return errors.ErrUserIDNotValid
	}

	return nil
}

func (us *UserRequestSingle) Validate() error {
	if us.IIN == nil || us.UserID == nil || us.Name == nil {
		return errors.ErrSomethingMissing
	}
	if *us.UserID <= 0 {
		return errors.ErrUserIDNotValid
	}

	if *us.Name == "" {
		return errors.ErrNameRequired
	}

	return nil
}

type UserUpdateRequestSingle struct {
	UserID *int64  `json:"user_id"`
	IIN    *string `json:"IIN"`
	OldIIN *string `json:"OldIIN"`
	Name   *string `json:"name"`
}

func (up *UserUpdateRequestSingle) Validate() error {
	if up.IIN == nil || up.UserID == nil || up.OldIIN == nil || up.Name == nil {
		return errors.ErrSomethingMissing
	}
	if *up.UserID <= 0 {
		return errors.ErrUserIDNotValid
	}
	if *up.Name == "" {
		return errors.ErrNameRequired
	}

	return nil
}
