package egov

import (
	"encoding/xml"
	"github.com/google/uuid"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zanbar-debtors/internal"
	"gitlab.com/zanbar-debtors/internal/entity"
	errors "gitlab.com/zanbar-debtors/internal/errors"
	"gitlab.com/zanbar-debtors/pkg/utils"
	"go.mongodb.org/mongo-driver/mongo"
	"strconv"
	"time"
)

type useCase struct {
	repo internal.Reps

	parser internal.Parser
	// share a logger
	logg        *log.Entry
	apiKey      string
	contentType string
	egovUrl     string
	indexName   string
	foundKey    string

	// iin count
	innCount int
}

func New(repo internal.Reps, parser internal.Parser, apiKey string, contentType string, egovUrl string, indexName string, foundKey string, innCount int, logg *log.Logger) internal.UseCase {

	loggerContext := logg.WithFields(log.Fields{
		"LEVEL": "UseCase",
	})

	return &useCase{
		apiKey:      apiKey,
		contentType: contentType,
		egovUrl:     egovUrl,
		indexName:   indexName,
		foundKey:    foundKey,

		repo:   repo,
		parser: parser,

		innCount: innCount,
		logg:     loggerContext,
	}
}

func (u *useCase) validateCount(savedIIN []*entity.IIN, newIIN []*entity.IIN) ([]*entity.IIN, error) {
	res := utils.AppendWithoutDuplication(savedIIN, newIIN)
	if len(res) > u.innCount {
		return nil, errors.ErrTooManyIINs
	}

	return res, nil
}

func (u *useCase) UpdateDebts(userID int64, IINs []*entity.IIN) (*entity.User, error) {
	for _, val := range IINs {
		err := u.ValidateIin(*val.INN)
		if err != nil {
			return nil, err
		}
	}

	user, err := u.repo.GetUser(userID)

	if err != nil && err != mongo.ErrNoDocuments {

		u.logg.WithFields(log.Fields{
			"FUNC": "UpdateDebts",
		}).Errorf("UserID : %v -> Error: %s", userID, errors.ErrUserNotFound)

		return nil, err
	}
	if user == nil {
		user = &entity.User{
			UserID: &userID,
			IINs:   []*entity.IIN{},
		}
	}

	count, err := u.validateCount(user.IINs, IINs)
	if err != nil {
		u.logg.WithFields(log.Fields{
			"FUNC": "UpdateDebts/validateCount",
		}).Errorf("UserID : %v -> Error: %s", userID, err)

		return nil, err
	}

	if user == nil {
		user = &entity.User{
			UserID: &userID,
		}
	}

	user.IINs = count
	err = u.repo.SaveUser(user)
	if err != nil {
		return nil, err
	}
	// go func add new IIN to Debts Collections
	go u.updateDebts(count, userID, u.repo, u.logg)

	return user, nil
}

func (u *useCase) updateDebts(given []*entity.IIN, userID int64, repo internal.Reps, logg *log.Entry) {
	list, err := repo.GetDebtsList()
	if err != nil {
		logg.WithFields(log.Fields{
			"FUNC": "updateDebts",
		}).Errorf("could not get iins from debts services.")
		return
	}

	for _, val := range given {
		if !utils.ContainsString2(list, *val.INN) {
			_, err := u.GetDebts(*val.INN, userID)
			if err != nil {

				logg.WithFields(log.Fields{
					"FUNC": "updateDebts",
				}).Errorf("could not save new IIN.")
				return
			}
		}
	}
}

func (u *useCase) RemoveDebts(userID int64, IINs []string) (*entity.User, error) {
	user, err := u.repo.GetUser(userID)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			u.logg.WithFields(log.Fields{
				"FUNC": "RemoveDebts",
			}).Errorf("UserID : %v -> Error: %s", userID, errors.ErrUserNotFound)
			return nil, errors.ErrUserNotFound
		}
		return nil, err
	}

	for _, val := range IINs {
		err = u.ValidateIin(val)
		if err != nil {
			u.logg.WithFields(log.Fields{
				"FUNC": "RemoveDebts/validateIin",
			}).Errorf("UserID : %v -> Error: %s", userID, err)
			return nil, err
		}
	}

	var temp []*entity.IIN
	for _, val := range user.IINs {
		if !utils.ContainsString2(IINs, *val.INN) {
			temp = append(temp, val)
		}
	}

	ent := &entity.User{
		UserID: &userID,
		IINs:   temp,
	}
	err = u.repo.SaveUser(ent)
	if err != nil {
		return nil, err
	}

	return ent, nil
}

func (u *useCase) UpdateDebtsSingle(userID int64, iin, oldIin string, name string) (*entity.User, error) {
	// validate iin
	err := u.ValidateIin(iin)
	if err != nil {
		u.logg.WithFields(log.Fields{
			"FUNC": "UpdateDebtsSingle/validateIin",
		}).Errorf("UserID : %v -> Error: %s", userID, err)
		return nil, err
	}

	user, err := u.repo.GetUser(userID)
	if err != nil {
		if err == mongo.ErrNoDocuments {

			u.logg.WithFields(log.Fields{
				"FUNC": "UpdateDebtsSingle",
			}).Errorf("UserID : %v -> Error: %s", userID, errors.ErrUserNotFound)

			return nil, errors.ErrUserNotFound
		}
		return nil, err
	}

	var isValid bool
	for index, val := range user.IINs {
		if *val.INN == oldIin {
			isValid = true
			user.IINs[index].INN = &iin
			user.IINs[index].Name = &name
		}
	}

	if isValid {
		err = u.repo.SaveUser(user)
		if err != nil {
			return nil, err
		}

		go u.updateDebts(user.IINs, userID, u.repo, u.logg)
		return user, err
	}

	u.logg.WithFields(log.Fields{
		"FUNC": "UpdateDebtsSingle/valid",
	}).Errorf("UserID : %v -> Error: %s", userID, errors.ErrIINBadFormat)

	return nil, errors.ErrIINBadFormat
}

func (u *useCase) RemoveDebtsSingle(userID int64, iin string) (*entity.User, error) {
	user, err := u.repo.GetUser(userID)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			u.logg.WithFields(log.Fields{
				"FUNC": "RemoveDebtsSingle",
			}).Errorf("UserID : %v -> Error: %s", userID, errors.ErrUserNotFound)
			return nil, errors.ErrUserNotFound
		}

		return nil, err
	}

	index := -1
	for i, val := range user.IINs {
		if *val.INN == iin {
			index = i
		}
	}
	// if not found iin to delete
	if index == -1 {
		return nil, errors.ErrIINBadFormat
	} else {
		user.IINs[index] = user.IINs[len(user.IINs)-1] // Copy last element to index i.
		user.IINs[len(user.IINs)-1] = nil
		user.IINs = user.IINs[:len(user.IINs)-1]
		err = u.repo.SaveUser(user)
		if err != nil {
			return nil, err
		}
		return user, nil
	}
}

func (u *useCase) CreateDebtsSingle(userID int64, iin string, name string) (*entity.User, error) {
	err := u.ValidateIin(iin)
	if err != nil {
		u.logg.WithFields(log.Fields{
			"FUNC": "CreateDebtsSingle/validateIin",
		}).Errorf("UserID : %v -> Error: %s", userID, err)

		return nil, err
	}

	user, err := u.repo.GetUser(userID)
	if err != nil && err != mongo.ErrNoDocuments {
		return nil, err
	}

	if user == nil {
		user = &entity.User{
			UserID: &userID,
			IINs: []*entity.IIN{{
				INN:  &iin,
				Name: &name,
			}},
		}
	} else {
		if utils.ContainsString(user.IINs, iin) {
			return nil, errors.ErrIINAlreadyExists
		}

		if len(user.IINs) > 4 {
			return nil, errors.ErrTooManyIINs
		}

		user.IINs = append(user.IINs, &entity.IIN{
			INN:  &iin,
			Name: &name,
		})
	}

	err = u.repo.SaveUser(user)
	if err != nil {
		return nil, err
	}

	go u.updateDebts(user.IINs, userID, u.repo, u.logg)
	return user, nil
}

// ValidateIin would be better to move into delivery level
func (u *useCase) ValidateIin(iin string) error {

	if len([]rune(iin)) == 0 {
		return errors.ErrIINBadFormat
	}
	if len([]rune(iin)) != 12 {
		return errors.ErrIINBadFormat
	}
	if _, err := strconv.Atoi(iin); err != nil {
		return errors.ErrIINBadFormat
	}

	return nil
}

func (u *useCase) GetDebts(iin string, userID int64) ([]*entity.RespDebtorsRegistry, error) {
	var debts []*entity.RespDebtorsRegistry
	err := u.ValidateIin(iin)
	if err != nil {
		return nil, err
	}

	soapResponse, err := MakeRequest(iin, u.indexName, u.apiKey, u.egovUrl, u.contentType, u.logg)
	if err != nil {
		return nil, err
	}

	debtsInternal := &entity.Debts{
		IIN: iin,
	}

	if soapResponse.Body.RequestResponse.Response.ResponseInfo.Status.Code == u.foundKey {

		debtsInternal.Rows = soapResponse.Body.RequestResponse.Response.ResponseData.Data.Rows
		debtsInternal.Hash = utils.AsSha256(debtsInternal.Rows)

		check, err := u.repo.GetDebts(iin)
		if err != nil && err != mongo.ErrNoDocuments {
			return nil, err
		}

		err = u.repo.SaveDebts(debtsInternal)
		if err != nil {
			// log could not save response

			u.logg.WithFields(log.Fields{
				"FUNC": "GetDebts/saveDebts",
			}).Errorf("UserID : %v -> Error: %s", userID, err)

			// maybe notification

		}
		debts = u.parseSoapResponse(debtsInternal.Rows)
		// compare hash
		if check != nil {
			if check.Hash != debtsInternal.Hash {
				// go func background
				go func(parser internal.Parser, logg *log.Entry, userID int64) {
					err = parser.Notify(iin, userID)
					if err != nil {
						logg.WithFields(log.Fields{
							"FUNC": "GetDebts/notify",
						}).Errorf("UserID : %v -> Error: %s", userID, err)
					}
				}(u.parser, u.logg, userID)
			}
		}

		return debts, nil
	}

	if debts == nil {
		debts = []*entity.RespDebtorsRegistry{}
	}

	return debts, nil
}

func (u *useCase) GetRegisteredIINs(userID int64) (*entity.User, error) {
	user, err := u.repo.GetUser(userID)
	if err != nil {
		if err == mongo.ErrNoDocuments {
			return nil, errors.ErrUserNotFound
		}
		return nil, err
	}
	if user.IINs == nil {
		user.IINs = []*entity.IIN{}
	}

	return user, nil
}

func MakeRequest(iin, indexName, apiKey, egovUrl, contentType string, logg *log.Entry) (*entity.SoapResponse, error) {

	req := entity.ReqDebtorsRegistry{
		MessageId:   uuid.New().String(),
		MessageDate: time.Now().Format(time.RFC3339),
		IndexName:   indexName,
		ApiKey:      apiKey,
		IinOrBin:    iin,
	}

	replyBytes, err := callSoapRequest(req, egovUrl, contentType, logg)
	if err != nil {
		logg.WithFields(log.Fields{
			"FUNC": "MakeRequest",
		}).Errorf("IIN : %v -> Error: %s", iin, err)
		return nil, errors.ErrDebtorsNotResponding
	}

	soapResponse := &entity.SoapResponse{}
	if err := xml.Unmarshal(replyBytes, &soapResponse); err != nil {
		logg.WithFields(log.Fields{
			"FUNC": "MakeRequest",
		}).Errorf("IIN : %v -> Error: %s", iin, err)
		return nil, errors.ErrDebtorsDataCouldNotBeParsed
	}
	return soapResponse, nil
}
