package egov

import (
	"bytes"
	"crypto/tls"
	"fmt"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zanbar-debtors/internal/entity"
	"io/ioutil"
	"net/http"
	"strings"
	"text/template"
	"time"
	"unicode/utf8"
)

func (u *useCase) parseSoapResponse(soapResponse []*entity.Rows) []*entity.RespDebtorsRegistry {
	var ret []*entity.RespDebtorsRegistry

	for _, i := range soapResponse {
		iin := i.Iin
		name := strings.ToUpper(fmt.Sprintf("%s %s %s", i.Surname, i.Name, i.SecondName))
		category := strings.ToUpper(i.CategoryRu)
		info := strings.ToUpper(fmt.Sprintf("%s, %s, %s %s %s",
			i.DisaNameRu,
			i.DisaDepartmentNameRu,
			i.OffSurname,
			i.OffName,
			i.OffSecondName))

		date, _ := time.Parse(time.RFC3339, i.IlDate)
		organ := strings.ToUpper(i.IlOrganRu)
		recoverer := strings.ToUpper(i.RecovererTitle)
		ret = append(ret, &entity.RespDebtorsRegistry{
			InRegistry: true,
			Iin:        iin,
			Name:       name,
			Category:   category,
			Info:       info,
			Date:       date,
			Organ:      organ,
			Recoverer:  recoverer,
		})
	}
	return ret
}

func callSoapRequest(req entity.ReqDebtorsRegistry, egovUrl, contentType string, logg *log.Entry) ([]byte, error) {
	var soapRequestTemplate = `
		<soapenv:Envelope
			xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/"
			xmlns:soap="http://soap.opendata.egov.nitec.kz/">
			<soapenv:Header/>
			<soapenv:Body>
				<soap:request>
					<request>
						<requestInfo>
							<messageId>{{.MessageId}}</messageId>
							<messageDate>{{.MessageDate}}</messageDate>
							<indexName>{{.IndexName}}</indexName>
							<apiKey>{{.ApiKey}}</apiKey>
						</requestInfo>
						<requestData>
							<data xmlns:ns2pep="http://bip.bee.kz/SyncChannel/v10/Types/Request"
								xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
								xsi:type="ns2pep:RequestMessage">
								<iinOrBin>{{.IinOrBin}}</iinOrBin>
							</data>
						</requestData>
					</request>
				</soap:request>
			</soapenv:Body>
		</soapenv:Envelope>
	`
	templ, err := template.New("SOAPRequest").Parse(soapRequestTemplate)
	if err != nil {

		logg.WithFields(log.Fields{
			"FUNC": "callSoapRequest/template",
		}).Errorf("IIN : %v -> Error: %s", req.IinOrBin, err)

		return nil, err
	}

	soapRequest := &bytes.Buffer{}
	if err := templ.Execute(soapRequest, req); err != nil {
		return nil, err
	}

	httpClient := &http.Client{
		Timeout: 10 * time.Second,
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{
				InsecureSkipVerify: true,
			},
		},
	}

	httpResponse, err := httpClient.Post(egovUrl, contentType, soapRequest)
	if err != nil {

		logg.WithFields(log.Fields{
			"FUNC": "callSoapRequest/http/request",
		}).Errorf("IIN : %v -> Error: %s", req.IinOrBin, err)

		return nil, err
	}
	defer httpResponse.Body.Close()

	httpResponseBody, err := ioutil.ReadAll(httpResponse.Body)
	if err != nil {

		logg.WithFields(log.Fields{
			"FUNC": "callSoapRequest/body/read",
		}).Errorf("IIN : %v -> Error: %s", req.IinOrBin, err)
		return nil, err
	}

	return removeNonUTF8Bytes(httpResponseBody), nil
}

func removeNonUTF8Bytes(data []byte) []byte {
	return bytes.Map(removeNonUTF8, data)
}

var removeNonUTF8 = func(r rune) rune {
	if r == utf8.RuneError {
		return -1
	}
	return r
}
