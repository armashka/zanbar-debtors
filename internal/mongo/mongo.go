package mongo

import (
	"context"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zanbar-debtors/internal"
	"gitlab.com/zanbar-debtors/internal/entity"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type repository struct {
	client          *mongo.Client
	database        string
	collectionUsers string
	collectionDebts string
	logg            *log.Entry
}

func (r repository) GetDebts(iin string) (*entity.Debts, error) {
	coll := r.client.Database(r.database).Collection(r.collectionDebts)
	filter := bson.D{{"_id", iin}}
	result := &entity.Debts{}
	err := coll.FindOne(context.TODO(), filter).Decode(result)
	if err != nil {
		r.logg.WithFields(log.Fields{
			"FUNC": "GetDebts",
		}).Errorf("IIN : %v -> Error: %s", iin, err)
		return nil, err
	}

	return result, nil
}

func (r repository) GetDebtsList() ([]string, error) {
	coll := r.client.Database(r.database).Collection(r.collectionDebts)
	results, err := coll.Distinct(context.TODO(), "_id", bson.D{})
	if err != nil {

		r.logg.WithFields(log.Fields{
			"FUNC": "GetDistinctIINFromDebts",
		}).Errorf("Error: %s", err)

		return nil, err
	}
	var iins []string
	for _, iin := range results {
		iins = append(iins, iin.(string))
	}

	return iins, nil
}

func (r repository) SaveDebts(data *entity.Debts) error {
	coll := r.client.Database(r.database).Collection(r.collectionDebts)
	filter := bson.D{{"_id", data.IIN}}
	opts := options.Update().SetUpsert(true)

	update := bson.D{
		{"$set", bson.D{
			{"rows", data.Rows},
		}},
	}

	_, err := coll.UpdateOne(context.TODO(), filter, update, opts)
	if err != nil {
		r.logg.WithFields(log.Fields{
			"FUNC": "SaveDebts",
		}).Errorf("INN : %s -> Error: %s", data.IIN, err)
		return err
	}

	return nil
}

func (r repository) GetDistinctIIN() ([]string, error) {
	coll := r.client.Database(r.database).Collection(r.collectionUsers)
	results, err := coll.Distinct(context.TODO(), "IINs.IIN", bson.D{})
	if err != nil {

		r.logg.WithFields(log.Fields{
			"FUNC": "GetDistinctIIN",
		}).Errorf("Error: %s", err)

		return nil, err
	}
	var iins []string
	for _, iin := range results {
		iins = append(iins, iin.(string))
	}

	return iins, nil
}

func (r repository) GetUser(id int64) (*entity.User, error) {
	coll := r.client.Database(r.database).Collection(r.collectionUsers)
	filter := bson.D{{"_id", id}}
	result := &entity.User{}
	err := coll.FindOne(context.TODO(), filter).Decode(result)
	if err != nil {
		r.logg.WithFields(log.Fields{
			"FUNC": "GetUser",
		}).Errorf("UserID : %v -> Error: %s", id, err)
		return nil, err
	}

	return result, nil
}

func (r repository) SaveUser(user *entity.User) error {

	coll := r.client.Database(r.database).Collection(r.collectionUsers)
	filter := bson.D{{"_id", user.UserID}}
	opts := options.Update().SetUpsert(true)

	update := bson.D{
		{"$set", bson.D{
			{"IINs", user.IINs},
		}},
	}

	_, err := coll.UpdateOne(context.TODO(), filter, update, opts)
	if err != nil {

		r.logg.WithFields(log.Fields{
			"FUNC": "SaveUser",
		}).Errorf("UserID : %v -> Error: %s", *user.UserID, err)

		return err
	}

	return nil
}

func (r repository) GetUserIDByIIN(iin string) ([]int64, error) {
	// TODO pass context - remove TODO()
	coll := r.client.Database(r.database).Collection(r.collectionUsers)
	filter := bson.D{{"IINs.IIN", iin}}

	var results []int64
	cur, err := coll.Find(context.TODO(), filter)
	if err != nil {

		r.logg.WithFields(log.Fields{
			"FUNC": "GetUserIDByIIN",
		}).Errorf("IIN : %v -> Error: %s", iin, err)

		return nil, err
	}

	for cur.Next(context.TODO()) {
		// create a value into which the single document can be decoded
		elem := &entity.User{}
		err := cur.Decode(elem)
		if err != nil {

			r.logg.WithFields(log.Fields{
				"FUNC": "GetUserIDByIIN/decode",
			}).Errorf("IIN : %v -> Error: %s", iin, err)

			return nil, err
		}

		results = append(results, *elem.UserID)
	}

	if err := cur.Err(); err != nil {
		return nil, err
	}

	// Close the cursor once finished
	cur.Close(context.TODO())

	return results, nil
}

func NewRep(client *mongo.Client, database string, collectionUsers string, collectionDebts string, logg *log.Logger) internal.Reps {

	loggerContext := logg.WithFields(log.Fields{
		"LEVEL": "Reps",
	})

	return &repository{
		client:          client,
		database:        database,
		collectionDebts: collectionDebts,
		collectionUsers: collectionUsers,
		logg:            loggerContext,
	}
}
