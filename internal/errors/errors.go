package errors

import "errors"

type Err struct {
	Err  string
	Code int
}

func (err Err) Error() string {
	return err.Err
}

func New(message string, code int) *Err {
	return &Err{
		Err:  message,
		Code: code,
	}
}

var ErrIINRequired = New("IinRequired", 400)
var ErrIINBadFormat = New("IinBadFormat", 400)
var ErrIINAlreadyExists = New("IinAlreadyExists", 400)
var ErrUserNotFound = New("UserNotFound", 400)
var ErrDebtorsNotResponding = New("DebtorsNotResponding", 500)
var ErrDebtorsDataCouldNotBeParsed = New("DebtorsDataCouldNotBeParsed", 500)
var ErrTooManyIINs = New("TooManyIINs", 400)

var ErrInternal = errors.New("internal Server error")

var ErrBadFormat = New("InputNotValid", 400)
var ErrNameRequired = New("NameRequired", 400)
var ErrUserIDNotValid = New("UserIDNotValid", 400)
var ErrSomethingMissing = New("SomethingMissing", 400)
var ErrValidationFailed = New("ValidationFailed", 400)

type ResponseError struct {
	Error string `json:"error"`
}
