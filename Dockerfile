FROM golang:1.15-alpine3.12 AS builder


COPY ./ /gitlab.com/zanbar/debtors
WORKDIR /gitlab.com/zanbar/debtors

RUN go mod download && go get -u ./...
#RUN go-wrapper download
#RUN go-wrapper install
RUN CGO_ENABLED=0 GOOS=linux go build -o ./.bin/app ./cmd/server/main.go
FROM alpine:latest

RUN apk --no-cache add ca-certificates
WORKDIR /root/
COPY --from=builder /gitlab.com/zanbar/debtors/config/conf.yml ./config/conf.yml
COPY --from=builder /gitlab.com/zanbar/debtors/.bin/app .


EXPOSE 8080

CMD [ "./app"]
