package utils

import (
	"encoding/json"
	log "github.com/sirupsen/logrus"
	"gitlab.com/zanbar-debtors/internal/errors"
	"net/http"
)

func ResponseWithHttpError(w http.ResponseWriter, er error, code int, logg *log.Entry) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)
	errs := errors.ResponseError{
		Error: er.Error(),
	}
	err := json.NewEncoder(w).Encode(errs)
	if err != nil {
		logg.Println(err)
	}
}

func ResponseWithHttp(w http.ResponseWriter, msg interface{}, code int, logg *log.Entry) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(code)
	err := json.NewEncoder(w).Encode(msg)
	if err != nil {
		logg.Println(err)
	}
}
