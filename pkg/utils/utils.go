package utils

import (
	"crypto/sha256"
	"fmt"
	"gitlab.com/zanbar-debtors/internal/entity"
)

func ContainsString(s []*entity.IIN, e string) bool {
	for _, a := range s {
		if *a.INN == e {
			return true
		}
	}
	return false
}

func ContainsString2(s []string, e string) bool {
	for _, a := range s {
		if a == e {
			return true
		}
	}
	return false
}

func AppendWithoutDuplication(a []*entity.IIN, b []*entity.IIN) []*entity.IIN {

	check := make(map[string]*entity.IIN)
	d := append(a, b...)
	res := make([]*entity.IIN, 0)
	for _, val := range d {
		check[*val.INN] = val
	}

	for _, val := range check {
		res = append(res, val)
	}

	return res
}

func AsSha256(o interface{}) string {
	h := sha256.New()
	h.Write([]byte(fmt.Sprintf("%v", o)))

	return fmt.Sprintf("%x", h.Sum(nil))
}
